package com.nt.servlet;

import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

public class ServletTest extends HttpServlet {

    private int guid = 0;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        int newGuid = guid++;
        String xml = request.getParameter("xml");
        String xmlMessage = "<testRq>\n" +
                "\t<data>string</data>\n" +
                "\t<guid>" + newGuid + "</guid>\n" +
                "</testRq>";

        final AsyncContext asyncContext = request.startAsync();
        final PrintWriter printWriter = response.getWriter();

        asyncContext.start(() -> {
            String answer = startTask(xml, xmlMessage);
            printWriter.println(answer);
            asyncContext.complete();
        });
    }

    private String startTask(String xml, String xmlMessage) {

        String corrID = UUID.randomUUID().toString();

        Thread t1 = new Thread(() -> {
            MyDB database = new MyDB();
            database.insert(xml);
        });
        t1.start();

        Thread t2 = new Thread(() -> {
            TomCatProducer.runJMS(xmlMessage, corrID);
        });
        t2.start();

        JMSContext context = MQConnect.connect();
        Destination destination = context.createQueue("queue:///" + "TEST.OUT");
        javax.jms.JMSConsumer consumer = context.createConsumer(destination, "JMSCorrelationID='" + corrID + "'");
        String receivedMessage = consumer.receiveBody(String.class);
        context.close();
        consumer.close();

        return receivedMessage;
    }
}


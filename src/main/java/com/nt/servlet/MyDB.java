package com.nt.servlet;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;

class MyDB {

    void insert(String xml) {
        PreparedStatement preparedStatement = null;
        InitialContext initContext = null;
        Connection conn = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/mydb");
            try {
                conn = ds.getConnection();
                Class.forName("com.mysql.cj.jdbc.Driver");
                String sql = "INSERT INTO servletbase (request) Values (?)";
                preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1, xml);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                assert preparedStatement != null;
                conn.close();
                preparedStatement.close();
            }
        } catch (ClassNotFoundException | NamingException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                assert initContext != null;
                initContext.close();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }
}


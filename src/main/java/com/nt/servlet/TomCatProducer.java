package com.nt.servlet;

import javax.jms.*;

class TomCatProducer {

    static void runJMS(String xmlMessage, String corrID) {
        JMSContext context = MQConnect.connect();
        Destination destination = context.createQueue("queue:///" + "TEST.IN");
        try {
            TextMessage message = context.createTextMessage(xmlMessage);
            message.setJMSCorrelationID(corrID);
            javax.jms.JMSProducer producer = context.createProducer();
            producer.send(destination, message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            context.close();
        }
    }
}
